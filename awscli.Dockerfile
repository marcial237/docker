FROM ubuntu

ENV AWS_ACCESS_KEY_ID YOUR_ACCESS_KEY_ID
ENV AWS_SECRET_ACCESS_KEY YOUR_SECRET_ACCESS_KEY
ENV AWS_DEFAULT_REGION YOUR_DEFAULT_REGION

RUN apt-get update && \
    apt-get install -y python3-pip && \
    pip3 install awscli